import React from 'react';

class Nama extends React.Component {
  render() {
    return <td className="colNama"> {this.props.nama} </td>;
  }
}

class Harga extends React.Component {
  render() {
    return <td className="colHarga"> {this.props.harga} </td>;
  }
}

class Berat extends React.Component {
  render() {	
    return <td className="colBerat"> {this.props.berat} kg </td>;
  }
}

let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]

class HargaBuah extends React.Component {
  render() {
    return (      
      <div className="container">
        <h1 className="title">Tabel Harga Buah</h1>
        <table className="tabelHarga">
          <tr>
            <th className="colNama"> Nama  </th>
            <th className="colHarga"> Harga </th>
            <th className="colBerat"> Berat </th>
          </tr>

          {dataHargaBuah.map(el=> {
            return (            
              <tr>
                <Nama nama={el.nama}/>
                <Harga harga={el.harga}/>
                <Berat berat={el.berat/1000}/>
  	        </tr> 
            )
          })}
        </table>
      </div>
    )
  }
}

export default HargaBuah