import React from 'react';
//import logo from './logo.svg';
import './App.css';
import Routes from "./Tugas-15/Route";
import { BrowserRouter as Router } from "react-router-dom";
import { ThemeProvider } from "./Tugas-15/ThemeContext";


const App = () => {
  return (
    <div className="body">
      <Router>
        <ThemeProvider>
          <Routes />
        </ThemeProvider>
      </Router>
    </div>
  );
}

export default App;