import React from 'react';

class FormBeliBuah extends React.Component {
  render() {
    return (
      <div className="container">
	    <div className="form-container">
	      <h1 className="title">Form Pembelian Buah</h1>
	      
	      <form action="" className="form-input" id="form-beli-buah">
	        <div className="input-container">
	          <div className="label">          
	            <label for="name">Nama Pelanggan</label>
	          </div>
	          <div className="input"> 
	            <input type="text" id="name" name="name"/><br/>
	          </div>
	        </div>
	         
	        <div className="input-container">   
	          <div className="label">   
	            <label>Daftar Item</label>
	          </div>
	          <div className="input"> 
	            <input type="checkbox"  name="item"/> Semangka<br/>
	            <input type="checkbox"  name="item"/> Jeruk<br/>
	            <input type="checkbox"  name="item"/> Nanas<br/>
	            <input type="checkbox"  name="item"/> Salak<br/>
	            <input type="checkbox"  name="item"/> Anggur<br/> 
	          </div> 
	        </div>   

	        <div className="input-container">        
	          <input type="submit" className="button" value="Kirim"/>
	        </div>  
	      </form>        
	    </div>
	  </div>
	);
  }
}

export default FormBeliBuah