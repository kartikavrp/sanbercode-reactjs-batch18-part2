import React from "react";


class DataBuah extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataHargaBuah: [
        { nama: "Semangka", harga: 10000, berat: 1000 },
        { nama: "Anggur", harga: 40000, berat: 500 },
        { nama: "Strawberry", harga: 30000, berat: 400 },
        { nama: "Jeruk", harga: 30000, berat: 1000 },
        { nama: "Mangga", harga: 30000, berat: 500 },
      ],

      inputNama : "",
      inputHarga: "",
      inputBerat: "",
      indexForm : "",
      editMode  : false,
      error     : false,
    };
  }

  handleChange = (event) => {
    let name  = event.target.name;
    let value = event.target.value;

    switch (name) {
      case "inputHarga":
        if (isNaN(value)) {
          value = "";
        }
        break;

      case "inputBerat":
        if (isNaN(value)) {
          value = "";
        }
        break;
      default:
        break;
    }

    this.setState({
      [name]: value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const { inputNama, inputHarga, inputBerat } = this.state;

    if (inputNama && inputHarga && inputBerat) {
      const newDataHargaBuah = {
        nama: this.state.inputNama,
        harga: parseInt(this.state.inputHarga),
        berat: parseInt(this.state.inputBerat),
      };
      this.setState({
        dataHargaBuah: [...this.state.dataHargaBuah, newDataHargaBuah],
        inputNama: "",
        inputHarga: "",
        inputBerat: "",
        error: false,
      });
    } else {
      this.setState({
        error: true,
      });
    }
  };

  handleEditData = (index) => {
    const data = this.state.dataHargaBuah[index];
    this.setState({
      inputNama: data.nama,
      inputHarga: data.harga,
      inputBerat: data.berat,
      indexForm: index,
      editMode: true,
    });
  };

  handleEdit = (event) => {
    event.preventDefault();
    const { inputNama, inputHarga, inputBerat } = this.state;

    const index = this.state.indexForm;
    let dataHargaBuah = this.state.dataHargaBuah;

    if (inputNama && inputHarga && inputBerat) {
      const newDataHargaBuah = {
        nama: this.state.inputNama,
        harga: parseInt(this.state.inputHarga),
        berat: parseInt(this.state.inputBerat),
      };

      dataHargaBuah[index] = newDataHargaBuah;

      this.setState({
        dataHargaBuah: dataHargaBuah,
        inputNama: "",
        inputHarga: "",
        inputBerat: "",
        indexForm: "",
        editMode: false,
        error: false,
      });
    } else {
      this.setState({
        error: true,
      });
    }
  };

  handleDelete = (index) => {
    let dataHargaBuah = this.state.dataHargaBuah;
    dataHargaBuah.splice(index, 1);

    if (this.state.editMode) {
      this.setState({
        dataHargaBuah: dataHargaBuah,
        inputNama: "",
        inputHarga: "",
        inputBerat: "",
        indexForm: "",
        editMode: false,
        error: false,
      });
    } else {
      this.setState({
        dataHargaBuah: dataHargaBuah,
      });
    }
  };

  handleCancel = () => {
    this.setState({
      inputNama: "",
      inputHarga: "",
      inputBerat: "",
      indexForm: "",
      editMode: false,
    });
  };

  render() {
    return (
      <div className="container">
        <div className="form-data-container">
          <h1 className="title">Form Data Buah</h1>

          <form onSubmit={this.state.editMode ? this.handleEdit : this.handleSubmit} className="form-input">
            
              <div className="input-container">
                <div className="label">          
                  <label for="name">Nama </label>
                </div>
                <div className="input"> 
                  <input type="text" name="inputNama" value={this.state.inputNama} onChange={this.handleChange} id="name"/>
                </div>
              </div>

              <div className="input-container">
                <div className="label">          
                  <label for="harga">Harga </label>
                </div>
                <div className="input"> 
                  <input type="text" name="inputHarga" value={this.state.inputHarga} onChange={this.handleChange} id="harga"/>
                </div>
              </div>    

              <div className="input-container">
                <div className="label">          
                  <label for="berat">Berat </label>
                </div>
                <div className="input"> 
                  <input type="text" name="inputBerat" value={this.state.inputBerat} onChange={this.handleChange} id="berat"/>
                </div>
              </div>       
             

              {this.state.error && (
                <div className="error">
                  Field tidak boleh kosong! <br/>
                </div>
              )}

              {this.state.editMode ? (
                <div className="enam">
                  <button className="btn-form" onClick={this.handleCancel}> Cancel </button>
                  <button className="btn-form" type="submit"> Edit </button>
                </div>
              ) : (
                <button className="btn-form" type="submit"> Tambah Data Baru </button>
              )}
            
          </form>
        </div>

        {/* Tabel Data Buah */}
        <h1 className="title">Tabel Data Buah</h1>
        <table className="tabelHarga">
          <thead>
            <tr>
              <th className="colNama">Nama</th>
              <th className="colHarga">Harga</th>
              <th className="colBerat">Berat</th>
              <th colSpan="2">Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.state.dataHargaBuah.map((data, i) => (
              <tr key={i}>
                <td className="colNama"> {data.nama}</td>
                <td className="colHarga">{data.harga}</td>
                <td className="colBerat">{parseFloat(data.berat / 1000)} kg</td>
                <td className="tujuh">
                  <button className="btn-edit" onClick={() => this.handleEditData(i)}> Edit </button>
                </td>
                <td className="tujuh">
                  <button className="btn-delete" onClick={() => this.handleDelete(i)}> Delete </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default DataBuah;
