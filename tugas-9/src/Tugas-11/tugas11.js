import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      timer: 100,
      date : new Date().toLocaleTimeString()
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({timer: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
      this.setState({
        timer: this.state.timer - 1 ,
        date : new Date().toLocaleTimeString()
      });
  }


  render(){    
    return(
      <div className="container">
        {
          this.state.timer >=0 &&(
            <>
              <div className="timer">                       
                <h1 className="current-time">
                    sekarang jam: {this.state.date}
                </h1>
                <h1 className="countdown">
                    hitung mundur: {this.state.timer}
                </h1> 
              </div>
            </>
          )
        }
      </div>
    )
  }
}

export default Timer
