import React, { useState, useEffect } from "react";
import Axios from "axios";

function DataBuah2() {
  const [fruits, setFruits]           = useState(null);
  const [idFruit, setIdFruit]         = useState("");
  const [input, setInput]             = useState({ name: "", price: "", weight: "" });
  const [editMode, setEditMode]       = useState(false);
  const [refreshData, setRefreshData] = useState(false);
  const [error, setError]             = useState(false);

  useEffect(() => {
    if (!refreshData) {
      const url = "http://backendexample.sanbercloud.com/api/fruits";
      Axios
        .get(url)
        .then((res) => {
          setFruits(res.data);
          setRefreshData(true);
        })
        .catch((err) => {
          console.log(err.response);
        });
    }
  }, [refreshData]);


  const handleChange = (event) => {
    let newInput = { ...input };
    let name     = event.target.name;
    let value    = event.target.value;

    switch (name) {
      case "price":
        if (isNaN(value)) {
          value = "";
        }
        break;

      case "weight":
        value = parseInt(value);
        if (isNaN(value)) {
          value = "";
        }
        break;
      default:
        break;
    }

    newInput[name] = value;
    setInput(newInput);
  };


  const handleSubmit = (event) => {
    event.preventDefault();
    const url = "http://backendexample.sanbercloud.com/api/fruits";
    const { name, price, weight } = input;

    if (name && price && weight) {
      Axios
        .post(url, input)
        .then((res) => {
          console.log(res);
          setRefreshData(false);
          setInput({ name: "", price: "", weight: "" });
          setError(false);
        })
        .catch((err) => {
          console.log(err.response);
        });
    } else {
      setError(true);
    }
  };


  const handleEditData = (id) => {
    const url = `http://backendexample.sanbercloud.com/api/fruits/${id}`;
    Axios
      .get(url)
      .then((res) => {
        setInput({
          name  : res.data.name,
          price : res.data.price,
          weight: res.data.weight,
        });
        setIdFruit(res.data.id);
        setEditMode(true);
        setError(false);
      })
      .catch((err) => {
        console.log(err.response);
      });
  };


  const handleEdit = (event) => {
    event.preventDefault();
    const url = `http://backendexample.sanbercloud.com/api/fruits/${idFruit}`;
    const { name, price, weight } = input;

    if (name && price && weight) {
      Axios
        .put(url, input)
        .then((res) => {
          console.log(res);
          setRefreshData(false);
          setInput({ name: "", price: "", weight: "" });
          setIdFruit("");
          setEditMode(false);
        })
        .catch((err) => {
          console.log(err.response);
        });
    } else {
      setError(true);
    }
  };

  const handleDelete = (id) => {
    const url = `http://backendexample.sanbercloud.com/api/fruits/${id}`;
    Axios
      .delete(url)
      .then((res) => {
        console.log(res);        
        setInput({ name: "", price: "", weight: "" });
        setIdFruit("");
        setRefreshData(false);
        setEditMode(false);
        setError(false);
      })
      .catch((err) => {
        console.log(err.response);
      });
  };

  const handleCancel = () => {
    setInput({ name: "", price: "", weight: "" });
    setIdFruit("");
    setEditMode(false);
    setError(false);
  };
  

  return ( 
  <div className="container">
    <div className="form-data-container">
      <h1 className="title">Form Data Buah</h1>

      <form onSubmit={editMode ? handleEdit : handleSubmit} className="form-input">
        
	      <div className="input-container">
	        <div className="label">          
	          <label for="name">Nama </label>
	        </div>
	        <div className="input"> 
	          <input type="text" name="name" value={input.name} onChange={handleChange} id="name"/>
	        </div>
	      </div>

	      <div className="input-container">
	        <div className="label">          
	          <label for="harga">Harga </label>
	        </div>
	        <div className="input"> 
	          <input type="text" name="price" value={input.price} onChange={handleChange} id="harga"/>
	        </div>
	      </div>    

	      <div className="input-container">
	        <div className="label">          
	          <label for="berat">Berat </label>
	        </div>
	        <div className="input"> 
	          <input type="text" name="weight" value={input.weight} onChange={handleChange} id="berat"/>
	        </div>
	      </div>       
	     
	      {error && (
	        <div className="error">
	          Field tidak boleh kosong! <br/>
	        </div>
	      )}

	      {editMode ? (
	        <div className="enam">
	          <button className="btn-form" onClick={handleCancel}> Cancel </button>
	          <button className="btn-form" type="submit"> Edit </button>
	        </div>
	      ) : (
	        <button className="btn-form" type="submit"> Tambah Data Baru </button>
	      )}        
      </form>
    </div>

    {/* Tabel Data Buah */}
    <h1 className="title">Tabel Data Buah</h1>
    <table className="tabelHarga">
      <thead>
        <tr>
          <th className="colNama">Nama</th>
          <th className="colHarga">Harga</th>
          <th className="colBerat">Berat</th>
          <th className="colAksi">Actions</th>
        </tr>
      </thead>
      <tbody>
      	{fruits &&
         fruits.map((data) => (
          <tr key={data.id}>
            <td className="colNama"> {data.name}</td>
            <td className="colHarga">{data.price}</td>
            <td className="colBerat">{parseFloat(data.weight/1000)} kg</td>
            <td className="colAksi">
              <button className="btn-edit" onClick={() => handleEditData(data.id)}> Edit </button>                      
              <button className="btn-delete" onClick={() => handleDelete(data.id)}> Delete </button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>  
  </div>
  );  
}

export default DataBuah2;