import React, { useEffect, useContext } from "react";
import { FruitsContext } from "./FruitsContext";
import Axios from "axios";

const FruitsList = () => {
  const {
    fruits, setFruits,
    idFruit, setIdFruit,
    input, setInput,
    refreshData, setRefreshData,
    editMode, setEditMode,
    error, setError,
  } = useContext(FruitsContext);


  useEffect(() => {
    if (!refreshData) {
      const url = "http://backendexample.sanbercloud.com/api/fruits";
      Axios
        .get(url)
        .then((res) => {
          setFruits(res.data);
          setRefreshData(true);
        })
        .catch((err) => {
          console.log(err.response);
        });
    }
  }, [refreshData]);


  const handleEditData = (id) => {
    const url = `http://backendexample.sanbercloud.com/api/fruits/${id}`;
    Axios
      .get(url)
      .then((res) => {
        setInput({
          name  : res.data.name,
          price : res.data.price,
          weight: res.data.weight,
        });
        setIdFruit(res.data.id);
        setEditMode(true);
        setError(false);
      })
      .catch((err) => {
        console.log(err.response);
      });
  };


  const handleDelete = (id) => {
    const url = `http://backendexample.sanbercloud.com/api/fruits/${id}`;
    Axios
      .delete(url)
      .then((res) => {
        console.log(res);        
        setInput({ name: "", price: "", weight: "" });
        setIdFruit("");
        setRefreshData(false);
        setEditMode(false);
        setError(false);
      })
      .catch((err) => {
        console.log(err.response);
      });
  };


  return ( 
    <> 
      <h1 className="title">Tabel Data Buah</h1>
      <table className="tabelHarga">
        <thead>
          <tr>
            <th className="colNama">Nama</th>
            <th className="colHarga">Harga</th>
            <th className="colBerat">Berat</th>
            <th className="colAksi">Actions</th>
          </tr>
        </thead>
        <tbody>
          {fruits &&
           fruits.map((data) => (
            <tr key={data.id}>
              <td className="colNama"> {data.name}</td>
              <td className="colHarga">{data.price}</td>
              <td className="colBerat">{parseFloat(data.weight/1000)} kg</td>
              <td className="colAksi">
                <button className="btn-edit" onClick={() => handleEditData(data.id)}> Edit </button>                      
                <button className="btn-delete" onClick={() => handleDelete(data.id)}> Delete </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>  
    </>
  );  
}

export default FruitsList