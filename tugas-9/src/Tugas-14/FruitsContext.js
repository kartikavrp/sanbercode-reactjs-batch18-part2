import React, { useState, createContext } from "react";

export const FruitsContext = createContext();

export const FruitsProvider = (props) => {
  const [fruits, setFruits] = useState([]);
  const [idFruit, setIdFruit] = useState("");
  const [input, setInput] = useState({ name: "", price: "", weight: "" });
  const [refreshData, setRefreshData] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [error, setError] = useState(false);

  return (
    <FruitsContext.Provider
      value = {{
        fruits, setFruits,
        idFruit, setIdFruit,
        input, setInput,
        refreshData, setRefreshData,
        editMode, setEditMode,
        error, setError,
      }}
    >
      {props.children}
    </FruitsContext.Provider>
  );
};