import React, { useEffect, useContext } from "react";
import { FruitsContext } from "./FruitsContext";
import Axios from "axios";

const FruitsForm = () => {
  const {
    idFruit, setIdFruit,
    input, setInput,
    refreshData, setRefreshData,
    editMode, setEditMode,
    error, setError,
  } = useContext(FruitsContext);

  const handleChange = (event) => {
    let newInput = { ...input };
    let name     = event.target.name;
    let value    = event.target.value;

    switch (name) {
      case "price":
        if (isNaN(value)) {
          value = "";
        }
        break;

      case "weight":
        value = parseInt(value);
        if (isNaN(value)) {
          value = "";
        }
        break;
      default:
        break;
    }
    newInput[name] = value;
    setInput(newInput);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const url = "http://backendexample.sanbercloud.com/api/fruits";
    const { name, price, weight } = input;

    if (name && price && weight) {
      Axios
        .post(url, input)
        .then((res) => {
          console.log(res);
          setRefreshData(false);
          setInput({ name: "", price: "", weight: "" });
          setError(false);
        })
        .catch((err) => {
          console.log(err.response);
        });
    } else {
      setError(true);
    }
  };

  const handleEdit = (event) => {
    event.preventDefault();
    const url = `http://backendexample.sanbercloud.com/api/fruits/${idFruit}`;
    const { name, price, weight } = input;

    if (name && price && weight) {
      Axios
        .put(url, input)
        .then((res) => {
          console.log(res);
          setRefreshData(false);
          setInput({ name: "", price: "", weight: "" });
          setIdFruit("");
          setEditMode(false);
        })
        .catch((err) => {
          console.log(err.response);
        });
    } else {
      setError(true);
    }
  };

  const handleCancel = () => {
    setInput({ name: "", price: "", weight: "" });
    setIdFruit("");
    setEditMode(false);
    setError(false);
  };


  return ( 
    <> 
      <div className="form-data-container">
        <h1 className="title">Form Data Buah</h1>

        <form onSubmit={editMode ? handleEdit : handleSubmit} className="form-input">        
	      <div className="input-container">
	        <div className="label">          
	          <label for="name">Nama </label>
	        </div>
	        <div className="input"> 
	          <input type="text" name="name" value={input.name} onChange={handleChange} id="name"/>
	        </div>
	      </div>

	      <div className="input-container">
	        <div className="label">          
	          <label for="harga">Harga </label>
	        </div>
	        <div className="input"> 
	          <input type="text" name="price" value={input.price} onChange={handleChange} id="harga"/>
	        </div>
	      </div>    

	      <div className="input-container">
	        <div className="label">          
	          <label for="berat">Berat </label>
	        </div>
	        <div className="input"> 
	          <input type="text" name="weight" value={input.weight} onChange={handleChange} id="berat"/>
	        </div>
	      </div>       
	     
	      {error && (
	        <div className="error">
	          Field tidak boleh kosong! <br/>
	        </div>
	      )}

	      {editMode ? (
	        <div className="enam">
	          <button className="btn-form" onClick={handleCancel}> Cancel </button>
	          <button className="btn-form" type="submit"> Edit </button>
	        </div>
	      ) : (
	        <button className="btn-form" type="submit"> Tambah Data Baru </button>
	      )}        
        </form>
      </div>
    </>
  );  
}

export default FruitsForm
