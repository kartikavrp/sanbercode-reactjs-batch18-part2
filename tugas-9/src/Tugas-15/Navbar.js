import React from "react";
import { Link } from "react-router-dom";
import Theme from "./Theme";

const Navbar = (props) => {
  return (
    <nav>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/tugas-9">Tugas 9</Link>
        </li>
        <li>
          <Link to="/tugas-10">Tugas 10</Link>
        </li>
        <li>
          <Link to="/tugas-11">Tugas 11</Link>
        </li>
        <li>
          <Link to="/tugas-12">Tugas 12</Link>
        </li>
        <li>
          <Link to="/tugas-13">Tugas 13</Link>
        </li>
        <li>
          <Link to="/tugas-14">Tugas 14</Link>
        </li>
        
        <li style={{ marginLeft: "auto" }}>
          <Theme />
        </li>
        <li>{props.theme}</li>
      </ul>
    </nav>
  );
};

export default Navbar;
