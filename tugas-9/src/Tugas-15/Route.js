import React, { useContext } from "react";
import { Switch, Route } from "react-router-dom";
import { ThemeContext } from "./ThemeContext";
import Navbar from "./Navbar";
import FormBeliBuah from '../Tugas-9/FormBeliBuah';
import HargaBuah from '../Tugas-10/daftarHarga';
import Timer from '../Tugas-11/tugas11';
import DataBuah from '../Tugas-12/tugas12';
import DataBuah2 from '../Tugas-13/tugas13';
import Fruits from '../Tugas-14/Fruits';
import Home from '../Tugas-15/Home';

const Routes = () => {
  const [theme] = useContext(ThemeContext);

  return (
    <div className={theme === "Light" ? "light-theme" : "dark-theme"}>
      <Navbar theme={theme === "Light" ? "Dark Theme" : "Light Theme"} />
      <div className="App">
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/tugas-9">
            <FormBeliBuah />
          </Route>
          <Route exact path="/tugas-10">
            <HargaBuah />
          </Route>
          <Route exact path="/tugas-11">
            <Timer start={101}/>
          </Route>
          <Route exact path="/tugas-12">
            <DataBuah  />
          </Route>
          <Route exact path="/tugas-13">
            <DataBuah2 />
          </Route>
          <Route exact path="/tugas-14">
            <Fruits />
          </Route>
          
        </Switch>
      </div>
    </div>
  );
};

export default Routes;
