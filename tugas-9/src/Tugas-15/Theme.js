import React, { useContext } from "react";
import { ThemeContext } from "./ThemeContext";

const Theme = () => {
  const [theme, setTheme] = useContext(ThemeContext);

  const changeTheme = (event) => {
    const checked = event.target.checked;
    const theme = checked ? "Dark" : "Light";
    setTheme(theme);
  };

  return (
    <div>
      <label className="switch">
        <input name="slider" type="checkbox" onChange={changeTheme} />
        <span className="slider round" />
      </label>
    </div>
  );
};

export default Theme;