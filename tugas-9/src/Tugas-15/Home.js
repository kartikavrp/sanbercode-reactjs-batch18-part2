import React, {Component} from 'react'

class Home extends Component{
  render(){    
    return(
      <div className="container">
        <h1 className="hello"> Hello World </h1>
        <p className="home-desc"> Halaman ini adalah rekap tugas ReactJs Sanbercode Batch 18 Part 2 </p>

      </div>
    )
  }
}

export default Home
